import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:school_app/Helpers/Constants.dart';
import 'package:school_app/Helpers/Utils.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/headings_home.dart';


class RankWig extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(12,0,12,2),
            child: HeadingTitle(label: "Top 3 Ranks",color: Utils.fromHex("#F67B5A"), onTap: (){print("");},),
          ),
          Container(
            height: 138,
            width: double.infinity,
            child: ListView.builder(
                padding: const EdgeInsets.fromLTRB(8,0,8,12),
                itemCount: 10,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index){
                  return Container(
                      width: 110,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 0),
                        child: _Stories(),
                      )
                  );
                }),
          )

        ],
      ),
    );
  }
}

class _Stories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 70,
          child: ClipRRect(
              borderRadius: BorderRadius.circular(600),
              child: CachedNetworkImage(height: 70, width: 70, fit: BoxFit.cover, imageUrl:  profileUrl,)
          ),
        ),
        SizedBox(height: 8,),
        Center(child: Text("Sujay hosmani", textAlign: TextAlign.center,  maxLines: 2, overflow: TextOverflow.fade, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 13),)),
        SizedBox(height: 1,),
        Text("1st Tank (99.8%)",maxLines: 1, textAlign: TextAlign.center, overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 11),)
      ],
    );
  }
}