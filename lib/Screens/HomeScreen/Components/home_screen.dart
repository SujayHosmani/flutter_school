import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:school_app/Helpers/Constants.dart';
import 'package:school_app/Helpers/Utils.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/LatestAnnouncement.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/headings_home.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/latestIssues.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/latest_story.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/main_cards.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/morning_toast.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/parent_detail.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/upcoming_event.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.black87,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(height: 200, width: double.infinity, color: Colors.blue, child: CachedNetworkImage(fit: BoxFit.cover, imageUrl: demoUrl,),),
            ParentDetails(),
            SizedBox(height: 8,),
            MainTitles(),
            SizedBox(height: 8,),
            MorningToast(),
            SizedBox(height: 8,),
            UpcomingEvent(),
            SizedBox(height: 8,),
            LatestStory(),
            SizedBox(height: 8,),
            LatestAnnouncement(),
            SizedBox(height: 8,),
            Stack(
              children: [

                Container(
                  margin: const EdgeInsets.only(top: 30),
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10,30,10,10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            MainCard(label: "Issues",imgUrl: "issue.png",),
                            SizedBox(width: 5,),
                            MainCard(label: "Raise an issue",imgUrl: "raise.png",),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  width: double.infinity,
                  decoration: new BoxDecoration(
                    gradient: Utils.btnGradient,
                  ),
                  child: TextButton(
                    child: new Text('All Issues', style: TextStyle(color: Colors.white),),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
            SizedBox(height: 8,),
            LatestIssues(),
            SizedBox(height: 12,)


          ],
        ),
      ),
    );
  }
}
