import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:school_app/Providers/global_provider.dart';
import 'package:school_app/Providers/student_provider.dart';
import 'package:school_app/Screens/HomeScreen/Components/home_screen.dart';
import 'package:school_app/Screens/LoginScreen/login_screen.dart';
import 'package:school_app/Screens/NavScreen/Component/nav_screen.dart';

import 'Helpers/auth_service.dart';
import 'Meeting/meeting_main.dart';
import 'Screens/LoginScreen/main_login_screen.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  AuthService appAuth = new AuthService();

  Widget _defaultHome = new MainLogin();

  bool _result = await appAuth.login();
  if (_result) {
    _defaultHome = new NavScreen();
  }
  runApp(MyApp(home: _defaultHome,));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final Widget home;

  const MyApp({Key key, this.home}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => GlobalProvider()),
        ChangeNotifierProvider(create: (context) => StudentProvider()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.orange,
          // textTheme: GoogleFonts.robotoTextTheme(Theme.of(context).textTheme)
          // .apply(bodyColor: Colors.black87,),
        ),
        home: home,
      ),
    );
  }
}

