

import 'package:flutter/cupertino.dart';
import 'package:school_app/Model/Student.dart';
import 'package:school_app/NetworkModule/api_path.dart';
import 'package:school_app/NetworkModule/api_response.dart';
import 'package:school_app/NetworkModule/dio_client.dart';
import 'package:school_app/NetworkModule/http_client.dart';

class StudentRepository {
  Future<CustomResponse<Student>> fetchStudentDetails(BuildContext context, String ph, String from, String aNo) async {
    final response = await HttpClient.instance.fetchData(context, APIPathHelper.getValue(APIPath.fetch_student, ph));
    CustomResponse<Student> ss = CustomResponse<Student>(Error: response?.Error, Status: response?.Status, Data: response?.Data != null ? Student.fromJson(response.Data) : null);
    return ss;
  }
}