import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:school_app/Helpers/Constants.dart';


class ParentDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 11, horizontal: 8),
        child: Row(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: CachedNetworkImage(height: 75, width: 75, fit: BoxFit.cover, imageUrl:  profileUrl,)
            ),
            SizedBox(width: 20,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text("Siddaraju", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                Text("Father of Ajay"),
                Text("4th class A section"),
                Text("More", style: TextStyle(color: Colors.orange),)
              ],
            )

          ],
        ),
      ),
    );
  }
}
