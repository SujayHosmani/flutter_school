import 'package:flutter/material.dart';

import '../../../Helpers/Utils.dart';

class SchoolBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      color: Utils.fromHex("#F67B5A"),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image(image: AssetImage("assets/images/school.png"), width: 60, height: 60,),
          Text("Sujay Schools public school", style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.w600),),
          Text("Agali", style: TextStyle(color: Colors.white70),),
          SizedBox(height: 15,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.phone, color: Colors.white,),
              SizedBox(width: 20,),
              Icon(Icons.mail, color: Colors.white,),
              SizedBox(width: 20,),
              Icon(Icons.map, color: Colors.white,),
            ],
          )
        ],
      ),
    );
  }
}
