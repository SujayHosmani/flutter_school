import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:school_app/Helpers/Constants.dart';
import 'package:school_app/Helpers/Utils.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/headings_home.dart';


class TrusteesWig extends StatelessWidget {
  final String title;

  const TrusteesWig({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(12,0,12,2),
            child: HeadingTitle(label: title,color: Utils.fromHex("#F67B5A"), onTap: (){print("");},),
          ),
          Container(
            height: 200,
            width: double.infinity,
            child: ListView.builder(
                padding: const EdgeInsets.fromLTRB(8,0,8,12),
                itemCount: 10,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index){
                  return Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 9),
                        child: _Trustee(title: title == "Campus" ? "Library" : "Sujay HS",),
                      )
                  );
                }),
          )

        ],
      ),
    );
  }
}

class _Trustee extends StatelessWidget {
  final String title;

  const _Trustee({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: CachedNetworkImage(height: 140, width: 142, fit: BoxFit.cover, imageUrl:  demoUrl,)
          ),
        ),
        SizedBox(height: 8,),
        Center(child: Text(title, textAlign: TextAlign.center,  maxLines: 2, overflow: TextOverflow.fade, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 13),)),
        SizedBox(height: 1,),
        Text("3rd floor",maxLines: 1, textAlign: TextAlign.center, overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 11),)
      ],
    );
  }
}


