import 'package:flutter/material.dart';


class MainTitles extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                MainCard(label: "Latest Stories",imgUrl: "story.png",),
                SizedBox(width: 5,),
                MainCard(label: "Chats",imgUrl: "chat.png",),
              ],
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                MainCard(label: "Events",imgUrl: "event.png",),
                SizedBox(width: 5,),
                MainCard(label: "Announcement",imgUrl: "announ.png",),
              ],
            )
          ],
        ),
      ),
    );
  }
}



class MainCard extends StatelessWidget {
  final String imgUrl;
  final String label;

  const MainCard({Key key, this.imgUrl, this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: (){},
        child: Card(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)), elevation: 2, child:
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(image:  AssetImage("assets/images/" + imgUrl), height: 30,),
              SizedBox(height: 3,),
              Text(label, style: TextStyle(fontSize: 12),)
            ],
          ),
        )),
      ),
    );
  }
}
