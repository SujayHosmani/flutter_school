import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:school_app/Helpers/Constants.dart';

class FounderDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 180),
      height: 194,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10))
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Image(image: AssetImage("assets/images/loc.png"), height: 20,),
                SizedBox(width: 6,),
                Text("Agali, Anantapur 515311", style: TextStyle(fontSize: 12), )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8,10,8,0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: CachedNetworkImage(imageUrl: profileUrl,height: 110,fit: BoxFit.cover, width: 98,),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Sujay HS", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),),
                        SizedBox(height: 2,),
                        Text("Founder of Sujay Public schools", style: TextStyle(fontSize: 11),),
                        SizedBox(height: 5,),
                        Text("Presidency Trust has been synonymous with quality, dedication and commitment in the field of education and the trustees aim to inspire and create achievers who will be better citizens of the world.", style: TextStyle(fontSize: 12), maxLines: 5, overflow: TextOverflow.ellipsis,),
                        Align(
                          alignment: Alignment.topRight,
                          child: TextButton(onPressed: (){}, style: TextButton.styleFrom(
                              padding: const EdgeInsets.all(0),
                              tapTargetSize: MaterialTapTargetSize.shrinkWrap
                          ), child: Text("More")),
                        )
                      ],
                    ),
                  ),
                ),

              ],
            ),
          )
        ],
      ),
    );
  }
}
