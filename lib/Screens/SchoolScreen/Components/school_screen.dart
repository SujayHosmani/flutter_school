import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:school_app/Helpers/Constants.dart';
import 'package:school_app/Helpers/Utils.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/headings_home.dart';
import 'package:school_app/Screens/SchoolScreen/Widgets/campus_widget.dart';
import 'package:school_app/Screens/SchoolScreen/Widgets/faculty_widget.dart';
import 'package:school_app/Screens/SchoolScreen/Widgets/fee_structure.dart';
import 'package:school_app/Screens/SchoolScreen/Widgets/feedback_widget.dart';
import 'package:school_app/Screens/SchoolScreen/Widgets/trustees.dart';

import '../Widgets/top_school_details.dart';
import '../Widgets/founder_details.dart';

class SchoolScreen extends StatefulWidget {
  @override
  _SchoolScreenState createState() => _SchoolScreenState();
}

class _SchoolScreenState extends State<SchoolScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.black87,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                SchoolBanner(),
                FounderDetails(),

              ],
            ),
            SizedBox(height: 8,),
            FacultyWig(),
            SizedBox(height: 8,),
            CampusWig(title: "Campus",),
            SizedBox(height: 8,),
            Stack(
              children: [
                Column(
                  children: [
                    FeedbackWidget(),
                    SizedBox(height: 30,)
                  ],
                ),
                Positioned(
                  bottom: 6,
                  left: 0,
                  right: 0,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(10,0,10,0),
                    width: double.infinity,
                    decoration: new BoxDecoration(
                      gradient: Utils.btnGradient,
                    ),
                    child: TextButton(
                      child: new Text('Give Us Feedback', style: TextStyle(color: Colors.white),),
                      onPressed: () {},
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 8,),
            TrusteesWig(title: "Trustees",),
            SizedBox(height: 8,),
            FeeStructure(),
            SizedBox(height: 8,),
            Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(12,0,12,12),
                child: Column(
                  children: [
                    HeadingTitle(label: "Boarding", color: Utils.peach, onTap: (){print("");},),
                  ],
                ),
              ),
            ),
            SizedBox(height: 8,),
            Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(12,0,12,12),
                child: Column(
                  children: [
                    HeadingTitle(label: "Gallery", color: Utils.peach, onTap: (){print("");},),
                  ],
                ),
              ),
            ),
            SizedBox(height: 8,),



          ],
        ),
      ),
    );
  }
}
