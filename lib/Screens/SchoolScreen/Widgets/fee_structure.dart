import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:school_app/Helpers/Constants.dart';
import 'package:school_app/Helpers/Utils.dart';
import 'package:school_app/Screens/HomeScreen/Widgets/headings_home.dart';


class FeeStructure extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12,0,12,12),
        child: Column(
          children: [
            HeadingTitle(label: "Fee Structure", color: Utils.peach, onTap: (){print("");},),
            Stack(
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(2),
                    child: CachedNetworkImage(height: 220, width: double.infinity, fit: BoxFit.cover, imageUrl: feeImg,)
                ),
              ],
            )

          ],
        ),
      ),
    );
  }
}
